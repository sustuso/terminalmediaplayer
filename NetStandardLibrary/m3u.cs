﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using M3UParser;
using M3UParser.Models;
using Newtonsoft.Json;

namespace NetStandardLibrary
{
    public class m3u
    {
        
        public bool Loaded = false;
        
        [MethodImpl(MethodImplOptions.NoInlining)]
        private string GetCurrentMethod()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }

        public M3uPlaylist m3uList = null;
        public m3u()
        {
            m3uList = new M3uPlaylist();
        }

        public async Task LoadFromUrl(string url)
        {
            try
            {
                var res = await Task.Run(() => LoadFromUrlAsync(new Uri(url)));
                Loaded = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR - " + ex.Message);
            }
        }


        public async Task<bool> LoadFromUrlAsync(Uri url)
        {
            try
            {
                using (var client = new System.Net.WebClient())
                {

                    var res = await client.DownloadStringTaskAsync(url);
                    if (res != null)
                    {
                        //verificare la lista
                        m3uList = await Task.Run(() => M3uParser.GetFromStream(GenerateStreamFromString(CheckM3U(res))));
                        Loaded = true;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public async Task LoadFileAsync(string filename)
        {
            try
            {
                var res = await Task.Run(() => LoadFromFile(filename));
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR - " + ex.Message);
            }
        }
        private string LoadFromFile(string filename)
        {
            if (File.Exists(filename))
            {
                try
                {
                    string contents = File.ReadAllText(filename);
                    contents = CheckM3U(contents);
                    File.WriteAllText("data.chk", contents);
                    using (FileStream fs = File.OpenRead("data.chk"))
                        m3uList = M3uParser.GetFromStream(fs);
                    Loaded = true;
                    return "ok";
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
            else
                return "File not Exist";
        }
        private string CheckM3U(string str)
        {

            //string text = str.Replace(System.Environment.NewLine + System.Environment.NewLine, System.Environment.NewLine + "new line" + System.Environment.NewLine);
            string text = str.Replace("\n\r\n", "\n newline \r\n");

            //File.WriteAllText("data.chk", text);
            return text;
        }
        public  void AddUserAgent(string useragent)
        {
            try
            { 
                foreach(var element in m3uList.PlaylistEntries)
                {
                    element.Uri = element.Uri + useragent;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR "+ GetCurrentMethod() + ex.Message);
            }

        }
        public void ExcludeGroup(string json)
        {
            try
            {
                List<string> exclude = JsonConvert.DeserializeObject<List<string>>(json);
                foreach (var e in exclude)
                    m3uList.PlaylistEntries = m3uList.PlaylistEntries.Where(x => !x.Group.ToLower().Contains(e.ToLower())).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR " + GetCurrentMethod() + ex.Message);
            }

        }
        public void ExcludeChanel(string json)
        {
            try
            { 
                List<string> exclude = JsonConvert.DeserializeObject<List<string>>(json);
                foreach (var e in exclude)
                    m3uList.PlaylistEntries = m3uList.PlaylistEntries.Where(x => !x.Name.ToLower().Contains(e.ToLower())).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR " + GetCurrentMethod() + ex.Message);
            }

        }
        public void SaveToFile(string filename)
        {
            try
            {
                if (File.Exists(filename))
                    File.Delete(filename);
                var stream = M3uParser.CreateStream(m3uList);
                var fileStream = File.Create(filename);
                Console.WriteLine("File -> " + filename);
                stream.Seek(0, SeekOrigin.Begin);
                stream.CopyTo(fileStream);
                fileStream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR "+ GetCurrentMethod() + ex.Message);
            }
        }

        private static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }


    }
}
