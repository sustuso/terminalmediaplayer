﻿using System;
using System.Collections.Generic;
using Terminal.Gui;
using NetStandardLibrary;
using System.Threading;
using Newtonsoft.Json;
using System.IO;

using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Emby.XmlTv.Classes;
using Nito.AsyncEx;

namespace TerminalMediaPlayer
{
    static class Program
    {
        static ListView lv;
        static Window win;
        static Toplevel top;
        static FrameView fBottom;
        static FrameView fIptv;
        static FrameView fSearch;
        static TextField tSearch;
        static FrameView fEpg;
        static TextField messageText;
        static TextView tEpg;
        static CheckBox cEpg;
        static int selected = 0;
        static Configuration conf = new Configuration();
        static CancellationTokenSource cToken = new CancellationTokenSource();
        static m3u M3U = new m3u();
        static List<string> totalChans = new List<string>();
        static List<string> visibleChans = new List<string>();
        static bool test = false;
        static int hourAdd = 0;
        static XmlTvReader epg;
        static void Main(string[] args)
        {

            if (args.Length > 0)
            {
                Console.WriteLine("Startup parameters found");
                if (args[0].ToLower() == "test")
                    test = true;
            }

            if (!LoadConfFile())
                return;

            AsyncContext.Run(() => MainAsync(args));

            LoadIptv();

            CreateGUI();

        }
        static async void MainAsync(string[] args)
        {

            await DowloadEPG();

            /*
            await M3U.LoadFileAsync("tv_channels_video1859_plus.m3u");
            listacanali = M3U.m3uList.PlaylistEntries.Select(x => x.Name).ToList();
            CreateGUI();
            */
        }
        public static void CreateGUI()
        {
            Application.Init();
            top = Application.Top;
            // Creates the top-level window to show

            win = new Window("Terminal Media Player")
            {
                X = 0,
                Y = 1, // Leave one row for the toplevel menu

                Width = Dim.Fill(),
                Height = Dim.Fill()
            };

            top.Add(win);
            fIptv = new FrameView(new Rect(2, 0, conf.Width, conf.Height), "Iptv");
            lv = new ListView(new Rect(2, 0, conf.Width - 4, conf.Height - 2), visibleChans);
            lv.SelectedChanged += SelectedChanged;
            fIptv.Add(lv);
            win.Add(fIptv);

            fEpg = new FrameView(new Rect(conf.Width + 3, 4, conf.Width, conf.Height - 4), "Epg");
            tEpg = new TextView(new Rect(0, 0, conf.Width - 2, conf.Height - 7));
            fEpg.Add(tEpg);
            win.Add(fEpg);
            fEpg.CanFocus = false;
            tEpg.CanFocus = false;

            fBottom = new FrameView(new Rect(conf.Width + 3, 0, conf.Width, 4), "Menu");

            var buttonPlay = new Button(0, 0, "Play");
            buttonPlay.Clicked += Play;
            fBottom.Add(buttonPlay);

            cEpg = new CheckBox(20, 0, "Epg", conf.EPGBool);
            fBottom.Add(cEpg);

            var buttonIptv = new Button(0, 1, "Open File");
            buttonIptv.Clicked += OpenFile;
            fBottom.Add(buttonIptv);

            var buttonQuit = new Button(20, 1, "Quit");
            buttonQuit.Clicked += Quit;
            fBottom.Add(buttonQuit);

            win.Add(fBottom);

            fSearch = new FrameView(new Rect(2, conf.Height, conf.Width * 2 + 1 , 3), "Search");
            tSearch = new TextField(2, 0, conf.Width, "");
            var buttonSearch = new Button(conf.Width + 3, 0, "Search");
            buttonSearch.Clicked += Search;
            Button clear = new Button(conf.Width + 15, 0, "Clear");
            clear.Clicked += Clear;
            fSearch.Add(tSearch);
            fSearch.Add(buttonSearch);
            fSearch.Add(clear);
            win.Add(fSearch);

            messageText = new TextField(2, conf.Height + 3, conf.Width*2 + 1, "");
            win.Add(messageText);
            Application.Run();
        }

        #region button_click
        public static void Clear()
        {
            try
            {
                visibleChans = totalChans;
                lv.SetSource(visibleChans);
                tSearch.Text = "";
            }
            catch (Exception ex)
            {
                messageText.Text = ex.Message;
            }
        }
        public static void Search()
        {
            try
            {
                //lv = new ListView(new Rect(2, 0, conf.Width - 4, conf.Height - 2), listacanali.Where(x => x.Contains("Rai 1")).ToList());
                visibleChans = totalChans.Where(x => x.ToUpper().Contains(tSearch.Text.ToString().ToUpper())).ToList();
                lv.SetSource(visibleChans);
            }
            catch (Exception ex)
            {
                messageText.Text = ex.Message;
            }
        }
        public static void Play()
        {
            try
            { //text.Text = Bash("/usr/bin/omxplayer http://vtech.ayproviders.xyz:80/video1859/169290/106295");
                var tchan = System.Text.RegularExpressions.Regex.Replace(visibleChans[selected], @"^[0-9]+ - ", "");
                var chan = M3U.m3uList.PlaylistEntries.Where(x => x.Name == tchan).FirstOrDefault();
                var url = chan.Uri;
                Bash($"/usr/bin/omxplayer {url}");
            }
            catch (Exception ex)
            {
                messageText.Text = ex.Message;
            }
        }
        public static void SelectedChanged()
        {
            try
            {
                selected = lv.SelectedItem;
                if (cEpg.Checked)
                {
                    cToken.Cancel();
                    cToken = new CancellationTokenSource();
                    var tchan = System.Text.RegularExpressions.Regex.Replace(visibleChans[selected], @"^[0-9]+ - ", "");
                    var first = epg.GetProgrammes(M3U.m3uList.PlaylistEntries.Where(x => x.Name == tchan).FirstOrDefault().Name.ToUpper(), DateTimeOffset.Now.AddHours(hourAdd), DateTimeOffset.Now.AddHours(5 + hourAdd), cToken.Token).FirstOrDefault();
                    if (first != null)
                    {
                        var description = WordWrap(first.Description, conf.Width - 2);
                        var str = $"{first.Title} \n" + $"{first.StartDate.AddHours(hourAdd).ToString("HH:mm")} - {first.EndDate.AddHours(hourAdd).ToString("HH:mm")} \n";
                        foreach (var line in description)
                            str = str + line + "\n";
                        tEpg.Text = str;
                    }
                    else
                        tEpg.Text = "";
                }
            }
            catch (Exception ex)
            {
                messageText.Text = ex.Message;
            }
        }
        public static void Quit()
        {
            top.Running = false;
        }
        public static void OpenFile()
        {
            try
            {
                var d = new OpenDialog("Open", "Open a file");
                if (!String.IsNullOrEmpty(conf.OpenFilePath))
                    d.DirectoryPath = conf.OpenFilePath;
                Application.Run(d);

                if (!d.Canceled)
                {    //MessageBox.Query(50, 7, "Selected File", string.Join(", ", d.FilePaths), "Ok");
                    messageText.Text = Bash("/usr/bin/omxplayer '" + string.Join(", ", d.FilePaths) + "'");

                }
                var index = string.Join(", ", d.FilePaths).LastIndexOf(@"/");
                conf.OpenFilePath = string.Join(", ", d.FilePaths).Substring(0, index);
            }
            catch (Exception ex)
            {
                messageText.Text = ex.Message;
            }

        }

        #endregion

        #region util
        public static List<string> WordWrap(string input, int maxCharacters)
        {
            List<string> lines = new List<string>();

            if (!input.Contains(" "))
            {
                int start = 0;
                while (start < input.Length)
                {
                    lines.Add(input.Substring(start, Math.Min(maxCharacters, input.Length - start)));
                    start += maxCharacters;
                }
            }
            else
            {
                string[] words = input.Split(' ');

                string line = "";
                foreach (string word in words)
                {
                    if ((line + word).Length > maxCharacters)
                    {
                        lines.Add(line.Trim());
                        line = "";
                    }

                    line += string.Format("{0} ", word);
                }

                if (line.Length > 0)
                {
                    lines.Add(line.Trim());
                }
            }

            return lines;
        }

        public static string Bash(this string cmd)
        {
            try
            {
                string result = "";
                using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
                {
                    proc.StartInfo.FileName = "/bin/bash";
                    proc.StartInfo.Arguments = "-c \" " + cmd + " \"";
                    proc.StartInfo.UseShellExecute = false;
                    proc.StartInfo.RedirectStandardOutput = true;
                    proc.StartInfo.RedirectStandardError = true;
                    proc.Start();

                    result += proc.StandardOutput.ReadToEnd();
                    result += proc.StandardError.ReadToEnd();

                    proc.WaitForExit();
                }
                return result;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private static async Task<bool> DowloadEPG()
        {
            DateTime thisTime = DateTime.Now;
            if (TimeZoneInfo.Local.IsDaylightSavingTime(thisTime))
                hourAdd = 1;

            if (!string.IsNullOrEmpty(conf.EPG))
            {
                Console.WriteLine("Epg Downloading");
                System.Net.WebClient client = new System.Net.WebClient();
                client.OpenRead(conf.EPG);
                Int64 bytes_total = Convert.ToInt64(client.ResponseHeaders["Content-Length"]);
                if (File.Exists("epg.gzip"))
                {
                    var length = new System.IO.FileInfo("epg.gzip").Length;
                    if (bytes_total == length)
                    {
                        return false;
                    }
                }
                await client.DownloadFileTaskAsync(new Uri(conf.EPG), "epg.gzip");
                Bash($"/bin/gunzip -c epg.gzip > guide.xml");
                return true;
            }
            try
            {
                if (File.Exists(conf.EPGFileName))
                {
                    epg = new XmlTvReader(conf.EPGFileName, "it");
                    return true;
                }
            }
            catch (Exception ex)
            {
                messageText.Text = ex.Message;
            }
            return false;
        }

        private static bool LoadConfFile()
        {
            if (!File.Exists(@"conf.json"))
            {
                Console.WriteLine("File conf.json not present");
                return false;
            }
            try
            {
                conf = JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(@"conf.json"));
                if (conf.Height == 0)
                    conf.Height = 40;
                if (conf.Width == 0)
                    conf.Width = 50;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private static bool LoadIptv()
        {
            if (!test)
            {
                if (!String.IsNullOrWhiteSpace(conf.Iptv))
                {
                    Console.Write($"Loading");
                    try
                    {
                        //M3U.LoadFileAsync("tv_channels_video1859_plus.m3u"); //test
                        M3U.LoadFromUrlAsync(new Uri(conf.Iptv)); //corretta
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
                else
                    return false;
            }
            if (!test)
            {

                int i = 0;
                while (!M3U.Loaded)
                {
                    Console.Write($"\r Loading {i++}");
                    Thread.Sleep(2000);
                }

                for (int j = 0; j < M3U.m3uList.PlaylistEntries.Count; j++)
                {
                    totalChans.Add($"{j} - {M3U.m3uList.PlaylistEntries[j].Name}");
                }

                visibleChans = totalChans;

                return true;
            }
            else
            {
                for (int j = 0; j < 100; j++)
                {
                    totalChans.Add($"{j} - Chan {j}");
                    visibleChans = totalChans;
                }
                return true;
            }
        }

        #endregion
    }

    public class Configuration
    {
        public string Iptv { get; set; }
        public string OpenFilePath { get; set; }
        public string EPG { get; set; }
        public bool EPGBool { get; set; }
        public string EPGFileName { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
